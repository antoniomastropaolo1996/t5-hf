# Importing stock libraries
import sys

import nlp
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader, RandomSampler, SequentialSampler
# Importing the T5 modules from huggingface/transformers
from transformers import T5Tokenizer, T5ForConditionalGeneration, T5Config

# WandB – Import the wandb library
import wandb
import os



# # Setting up the device for GPU usage
from torch import cuda

from core.custom_dataset import CustomDataset

device = 'cuda' if cuda.is_available() else 'cpu'

#Load fine-tuning dataset

Mutants_train = nlp.load_dataset("core/script/mutant_dataset_script.py", split="train")
BFsmall_train = nlp.load_dataset("core/script/bfp_dataset_script.py", split="train")

Mutants_test = nlp.load_dataset("core/script/mutant_dataset_script.py", split="test")
BFsmall_test = nlp.load_dataset("core/script/bfp_dataset_script.py", split="test")

#.............#


Mutants_map_train = Mutants_train.map(lambda example: {
    "source": 'generate mutant: %s </s>' % example['fixed'].lower(),
    "target": '%s </s>' % example['buggy'].lower()
    }, remove_columns=Mutants_train.column_names)


BFsmall_map_train = BFsmall_train.map(lambda example: {
    "source": 'generate small patch: %s </s>' % example['buggy'].lower(),
    "target": '%s </s>' % example['fixed'].lower()
    }, remove_columns=BFsmall_train.column_names)

Mutants_map_test = Mutants_test.map(lambda example: {
    "source": 'generate mutant: %s </s>' % example['fixed'].lower(),
    "target": '%s </s>' % example['buggy'].lower()
    }, remove_columns=Mutants_test.column_names)


BFsmall_map_test = BFsmall_test.map(lambda example: {
    "source": 'generate small patch: %s </s>' % example['buggy'].lower(),
    "target": '%s </s>' % example['fixed'].lower()
    }, remove_columns=BFsmall_test.column_names)


# Preparaing the multidataset which follows a proportional sampling according to the authors :

multidataset_train = nlp.build_multitask(BFsmall_map_train, Mutants_map_train)
multidataset_test =  nlp.build_multitask(BFsmall_map_test, Mutants_map_test)
#multidataset_eval =  nlp.build_multitask(BFsmall_map_eval, Mutants_map_eval)


source_input_train = []
target_output_train = []

source_input_test = []
target_output_test = []

source_input_eval = []
target_output_eval = []

for item in multidataset_train:
    source_input_train.append(item['source'])
    target_output_train.append(item['target'])

for item in multidataset_test:
    source_input_test.append(item['source'])
    target_output_test.append(item['target'])

"""for item in multidataset_train:
    source_input_eval.append(item['source'])
    target_output_eval.append(item['target'])
"""

train_dataset = pd.DataFrame(list(zip(source_input_train, target_output_train)), columns =['input', 'output'])
test_dataset = pd.DataFrame(list(zip(source_input_test, target_output_test)), columns = ['input', 'output'])
eval_dataset = pd.DataFrame(list(zip(source_input_eval, target_output_eval)), columns = ['input', 'output'])


# Creating the training function. This will be called in the main function. It is run depending on the epoch value.
# The model is put into train mode and then we wnumerate over the training loader and passed to the defined network

def train(epoch, tokenizer, model, device, loader, optimizer):
    model.train()
    for _, data in enumerate(loader, 0):
        y = data['target_ids'].to(device, dtype=torch.long)
        y_ids = y[:, :-1].contiguous()
        lm_labels = y[:, 1:].clone().detach()
        lm_labels[y[:, 1:] == tokenizer.pad_token_id] = -100
        ids = data['source_ids'].to(device, dtype=torch.long)
        mask = data['source_mask'].to(device, dtype=torch.long)

        outputs = model(input_ids=ids, attention_mask=mask, decoder_input_ids=y_ids, lm_labels=lm_labels)
        loss = outputs[0]

        if _ % 10 == 0:
            print(f'Training Loss {loss.item()}')
            wandb.log({"Training Loss": loss.item()})

        if _ % 500 == 0:
            print(f'Epoch: {epoch}, Loss:  {loss.item()}')

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # Uncomment the following two lines for TPU computation
        # xm.optimizer_step(optimizer)
        # xm.mark_step()


def validate(epoch, tokenizer, model, device, loader):
    model.eval()
    predictions = []
    actuals = []
    with torch.no_grad():
        for _, data in enumerate(loader, 0):

            y = data['target_ids'].to(device, dtype=torch.long)
            ids = data['input_ids'].to(device, dtype=torch.long)
            mask = data['attention_mask'].to(device, dtype=torch.long)

            # Change here for the decoding mechanism.
            # The implemented one is beam search where k = 3
            generated_ids = model.generate(
                input_ids=ids,
                attention_mask=mask,
                max_length=512,
                num_beams=3,
                early_stopping=True
            )

            preds = [tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in
                     generated_ids]
            target = [tokenizer.decode(t, skip_special_tokens=True, clean_up_tokenization_spaces=True) for t in y]

            if _ % 100 == 0:
                print(f'Completed {_}')

            predictions.extend(preds)
            actuals.extend(target)

    return predictions, actuals


def main():

    os.environ["WANDB_API_KEY"] = "b924723db8229439cffbb76bc4bcc2c9e4b59347"
    os.environ["WANDB_MODE"] = "run"

    # WandB – Initialize a new run
    wandb.init(project="T5_multitask_hf")

    # WandB – Config is a variable that holds and saves hyperparameters and inputs
    # Defining some key variables that will be used later on in the training
    config = wandb.config  # Initialize config
    config.TRAIN_BATCH_SIZE = 1
    config.VALID_BATCH_SIZE = 1
    config.TRAIN_EPOCHS = 2  # number of epochs to train (default: 10)
    config.VAL_EPOCHS = 1
    config.LEARNING_RATE = 1e-4  # learning rate (default: 0.001)
    config.SEED = 42  # random seed (default: 42)
    config.MAX_LEN = 512

    # Set random seeds and deterministic pytorch for reproducibility
    torch.manual_seed(config.SEED)  # pytorch random seed
    np.random.seed(config.SEED)  # numpy random seed
    torch.backends.cudnn.deterministic = True

    configFile = T5Config.from_json_file('./data/pretrained_model/config.json')
    tokenizer = T5Tokenizer.from_pretrained('./data/vocab/dl4se_vocab.model')

    # Defining the parameters for creation of dataloaders
    train_params = {
        'batch_size': config.TRAIN_BATCH_SIZE,
        'shuffle': True,
        'num_workers': 0
    }

    test_params = {
        'batch_size': config.VALID_BATCH_SIZE,
        'shuffle': False,
        'num_workers': 0
    }

    # Creating the Training and Validation dataset for further creation of Dataloader
    training_set = CustomDataset(train_dataset, tokenizer, config.MAX_LEN)
    test_set = CustomDataset(test_dataset, tokenizer, config.MAX_LEN)

    # Creation of Dataloaders for testing and validation. This will be used down for training and validation stage for the model.
    training_loader = DataLoader(training_set, **train_params)
    test_loader = DataLoader(test_set, **test_params)

    # Defining the model. We are using t5-base model and added a Language model layer on top for generation of Summary.
    # Further this model is sent to device (GPU/TPU) for using the hardware.
    model = T5ForConditionalGeneration.from_pretrained(
        './data/pretrained_model/model.bin',
        config=configFile
    ).to(device)

    # Defining the optimizer that will be used to tune the weights of the network in the training session.
    optimizer = torch.optim.Adam(params=model.parameters(), lr=config.LEARNING_RATE)

    # Log metrics with wandb
    wandb.watch(model, log="all")

    model_dir = "output_dir/"

    # Training loop
    print('Initiating Fine-Tuning for the model on our dataset')

    for epoch in range(config.TRAIN_EPOCHS):
        train(epoch, tokenizer, model, device, training_loader, optimizer)
        torch.save(model.state_dict(), os.path.join(model_dir, 'epoch-{}.pth'.format(epoch)))

    # Validation loop and saving the resulting file with predictions and acutals in a dataframe.
    # Saving the dataframe as predictions.csv
    print('Now generating summaries on our fine tuned model for the validation dataset and saving it in a dataframe')
    for epoch in range(config.VAL_EPOCHS):
        predictions, actuals = validate(epoch, tokenizer, model, device, test_loader)
        final_df = pd.DataFrame({'Generated Text': predictions, 'Actual Text': actuals})
        final_df.to_csv('./models/predictions.csv')
        print('Output Files generated for review')


if __name__ == '__main__':
    main()